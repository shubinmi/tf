package controller

import (
	"bitbucket.org/shubinmi/tf/app/model"
	"bitbucket.org/shubinmi/tf/app/repository"
	"bitbucket.org/shubinmi/tf/app/service/norm"
	"bitbucket.org/shubinmi/tf/app/service/valid"
	"bitbucket.org/shubinmi/tf/app/usecase"
	"context"
	"github.com/sirupsen/logrus"
)

type Url struct {
	validate  *valid.Url
	normalize *norm.Url
	logic     *usecase.Url
}

func NewUrl(repo repository.Writer, host string) *Url {
	return &Url{
		validate:  valid.NewUrl(),
		normalize: norm.NewUrl(host),
		logic:     usecase.NewUrl(repo),
	}
}

func (u *Url) Shrink(ctx context.Context, addr string) (res string, e error) {
	type shrinkStreamFunc func(context.Context, model.Url) (model.Url, error)
	m := model.Url{Address: addr}
	for i, f := range []shrinkStreamFunc{u.validate.Address, u.logic.Shrink, u.normalize.Alias} {
		logrus.Infof("shrink step %d in: mod = %+v ; err = %v", i, m, e)
		m, e = f(ctx, m)
		logrus.Infof("shrink step %d out: mod = %+v ; err = %v", i, m, e)
		if e != nil {
			return
		}
	}
	res = m.Alias
	return
}
