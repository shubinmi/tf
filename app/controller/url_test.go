package controller

import (
	"bitbucket.org/shubinmi/tf/app/repository"
	"context"
	"regexp"
	"strings"
	"testing"
)

func TestUrl_Shrink(t *testing.T) {
	u1 := NewUrl(&repository.UrlMock{}, "http://mo.ck/")
	u2 := NewUrl(&repository.UrlMock{}, "")
	u3 := NewUrl(&repository.UrlErrMock{}, "http://mo.ck/")
	tests := map[string]struct {
		url    *Url
		addr   string
		errHas string
	}{
		"positive":   {url: u1, addr: "http://do.it", errHas: ""},
		"invalid":    {url: u1, addr: "", errHas: "address is req"},
		"no host":    {url: u2, addr: "http://do.it", errHas: "wrong short domain"},
		"cannot add": {url: u3, addr: "http://do.it", errHas: "not created"},
	}
	ctx := context.Background()
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			r, e := test.url.Shrink(ctx, test.addr)
			if test.errHas == "" && e != nil {
				t.Fatal("got unexpected err", e)
			}
			if test.errHas != "" && e == nil {
				t.Fatal("got unexpected positive", e)
			}
			if e != nil {
				if !strings.Contains(e.Error(), test.errHas) {
					t.Fatal("err doesn't match", e, test.errHas)
				}
				return
			}
			if !strings.Contains(r, "http://mo.ck/") {
				t.Fatal("short has not domain")
			}
			r = strings.ReplaceAll(r, "http://mo.ck/", "")
			if len(r) != 6 {
				t.Fatal("short has wrong number of symbols")
			}
			isAlpha := regexp.MustCompile(`^[A-Za-z]+$`).MatchString
			if !isAlpha(r) {
				t.Fatal("short has wrong format of symbols")
			}
		})
	}
}
