package usecase

import (
	"bitbucket.org/shubinmi/tf/app/model"
	"bitbucket.org/shubinmi/tf/app/repository"
	"bitbucket.org/shubinmi/tf/pkg/cb"
	"bitbucket.org/shubinmi/tf/pkg/rand"
	"context"
	"github.com/pkg/errors"
)

type Url struct {
	repo repository.Writer
}

func NewUrl(repo repository.Writer) *Url {
	return &Url{repo: repo}
}

func (u *Url) Shrink(ctx context.Context, m model.Url) (r model.Url, e error) {
	e = cb.Call(func() error {
		m.Alias = rand.Str(6)
		r, e = u.repo.Add(ctx, m)
		return errors.Wrap(e, "not created")
	}, 10, 0)

	return r, e
}
