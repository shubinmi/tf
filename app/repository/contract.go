package repository

import (
	"bitbucket.org/shubinmi/tf/app/model"
	"context"
)

type Writer interface {
	Add(context.Context, model.Url) (model.Url, error)
}
