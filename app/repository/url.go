package repository

import (
	"bitbucket.org/shubinmi/tf/app/model"
	"bitbucket.org/shubinmi/tf/pkg/pg"
	"context"
	"github.com/pkg/errors"
)

type url struct {
	db *pg.Client
}

func NewUrl(db *pg.Client) *url {
	return &url{db: db}
}

func (u *url) Add(ctx context.Context, m model.Url) (r model.Url, e error) {
	q := pg.NewInsertQuery(pg.WithTable("urls"),
		pg.WithCol("address", m.Address),
		pg.WithCol("alias", m.Alias),
		pg.WithReturns("id"))
	e = u.db.Insert(ctx, q, &m.Id)
	if e != nil {
		e = errors.Wrapf(e, "add url: %v", m)
		return
	}
	r = m
	return
}
