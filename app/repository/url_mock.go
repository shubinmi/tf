package repository

import (
	"bitbucket.org/shubinmi/tf/app/model"
	"context"
	"errors"
)

type UrlMock struct {
}

func (u UrlMock) Add(_ context.Context, m model.Url) (model.Url, error) {
	return m, nil
}

type UrlErrMock struct {
}

func (u UrlErrMock) Add(_ context.Context, m model.Url) (model.Url, error) {
	return m, errors.New("mock err")
}
