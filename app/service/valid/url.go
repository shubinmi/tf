package valid

import (
	"bitbucket.org/shubinmi/tf/app/model"
	"context"
	"errors"
)

type Url struct {
}

func NewUrl() *Url {
	return &Url{}
}

func (v *Url) Address(_ context.Context, m model.Url) (model.Url, error) {
	if m.Address == "" {
		return m, errors.New("address is req")
	}
	return m, nil
}
