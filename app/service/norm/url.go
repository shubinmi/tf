package norm

import (
	"bitbucket.org/shubinmi/tf/app/model"
	"context"
	"github.com/pkg/errors"
)

type Url struct {
	host string
}

func NewUrl(host string) *Url {
	return &Url{host: host}
}

func (u *Url) Alias(_ context.Context, m model.Url) (model.Url, error) {
	if u.host == "" {
		return m, errors.New("wrong short domain")
	}
	m.Alias = u.host + m.Alias
	return m, nil
}
