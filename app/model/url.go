package model

type Url struct {
	Id      uint
	Address string
	Alias   string
}
