module bitbucket.org/shubinmi/tf

go 1.13

require (
	github.com/caarlos0/env/v6 v6.1.0
	github.com/golang-migrate/migrate/v4 v4.8.0
	github.com/golang/protobuf v1.3.3
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.0
	github.com/jackc/pgx v3.2.0+incompatible
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/net v0.0.0-20191002035440-2ec189313ef0 // indirect
	google.golang.org/genproto v0.0.0-20200207204624-4f3edf09f4f6 // indirect
	google.golang.org/grpc v1.27.1
	gopkg.in/yaml.v2 v2.2.3 // indirect
)
