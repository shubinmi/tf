package cb

import "time"

type Exec func() error

func Call(f Exec, n int, sleep time.Duration) (lastErr error) {
	for i := 0; i < n; i++ {
		if lastErr = f(); lastErr == nil {
			return
		}
		if sleep > 0 {
			time.Sleep(sleep)
		}
	}
	return
}
