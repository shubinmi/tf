package pg

import (
	"bitbucket.org/shubinmi/tf/internal/conf"
	"bitbucket.org/shubinmi/tf/pkg/cb"
	"context"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/log/log15adapter"
	"github.com/pkg/errors"
	"strconv"
)

type Client struct {
	pool *pgx.ConnPool
}

func NewClient(c conf.DB) (*Client, error) {
	port, err := strconv.ParseUint(c.Port, 10, 16)
	if err != nil {
		return nil, errors.Wrap(err, "bad bd port")
	}
	cconf := pgx.ConnConfig{
		Host:     c.Host,
		Port:     uint16(port),
		Database: c.Db,
		User:     c.User,
		Password: c.Pass,
		LogLevel: pgx.LogLevelInfo,
		Logger:   log15adapter.NewLogger(newLog()),
		RuntimeParams: map[string]string{
			"standard_conforming_strings": "on",
		},
		PreferSimpleProtocol: true,
	}
	pcnf := pgx.ConnPoolConfig{
		ConnConfig:     cconf,
		MaxConnections: 3,
	}
	p, err := pgx.NewConnPool(pcnf)
	if err != nil {
		return nil, errors.Wrap(err, "db conn pool")
	}
	return &Client{pool: p}, nil
}

func (c *Client) Close() {
	c.pool.Close()
}

func (c *Client) Insert(ctx context.Context, q Query, dest ...interface{}) (err error) {
	done := make(chan struct{})
	con, _ := c.conn(ctx, done)
	defer func() {
		done <- struct{}{}
	}()

	sql, args := q.Query()
	row := con.QueryRow(sql, args...)
	if err = (*pgx.Rows)(row).Err(); err != nil {
		err = errors.Wrap(err, "wrong query")
		return
	}
	if len(dest) == 0 {
		return
	}
	err = row.Scan(dest...)
	if err != nil {
		err = errors.Wrapf(err, "insert pg query: %s ; %v", sql, args)
	}
	return
}

func (c *Client) conn(ctx context.Context, done <-chan struct{}) (*pgx.Conn, error) {
	var (
		err error
		con *pgx.Conn
	)
	err = cb.Call(func() error {
		con, err = c.pool.Acquire()
		return err
	}, 5, 0)
	if err != nil {
		return nil, errors.Wrap(err, "acquire conn")
	}
	go func() {
		select {
		case <-ctx.Done():
			_ = con.Close()
		case <-done:
			c.pool.Release(con)
			return
		}
	}()
	return con, nil
}
