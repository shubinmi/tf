package pg

import "github.com/sirupsen/logrus"

type logger struct {
	log *logrus.Entry
}

func (l logger) Debug(msg string, ctx ...interface{}) {
	l.log.Debug(msg, ctx)
}

func (l logger) Info(msg string, ctx ...interface{}) {
	l.log.Info(msg, ctx)
}

func (l logger) Warn(msg string, ctx ...interface{}) {
	l.log.Warn(msg, ctx)
}

func (l logger) Error(msg string, ctx ...interface{}) {
	l.log.Error(msg, ctx)
}

func (l logger) Crit(msg string, ctx ...interface{}) {
	l.log.Panic(msg, ctx)
}

func newLog() *logger {
	lr := logrus.NewEntry(logrus.New())
	lr.Logger.SetFormatter(&logrus.JSONFormatter{})
	return &logger{
		log: lr,
	}
}
