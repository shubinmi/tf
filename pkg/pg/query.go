package pg

import (
	"fmt"
	"strconv"
)

type Query interface {
	Query() (string, []interface{})
}

type Table struct {
	Prefix string
	Name   string
}

type EscPair struct {
	Col string
	Val interface{}
}

type InsertQuery struct {
	table   Table
	cols    []EscPair
	returns []string
}

type optsFunc func(*InsertQuery)

func WithTable(table string) func(iq *InsertQuery) {
	return func(iq *InsertQuery) {
		iq.table.Name = table
	}
}

func WithCol(col string, val interface{}) func(iq *InsertQuery) {
	return func(iq *InsertQuery) {
		if iq.cols == nil {
			iq.cols = []EscPair{{Col: col, Val: val}}
			return
		}
		iq.cols = append(iq.cols, EscPair{Col: col, Val: val})
	}
}

func WithReturns(cols ...string) func(iq *InsertQuery) {
	return func(iq *InsertQuery) {
		iq.returns = cols
	}
}

func NewInsertQuery(opts ...optsFunc) *InsertQuery {
	q := &InsertQuery{}
	for _, f := range opts {
		f(q)
	}
	return q
}

func (q InsertQuery) Operation() string {
	return "INSERT INTO"
}

func (q InsertQuery) Table() string {
	return q.table.Prefix + q.table.Name
}

func (q InsertQuery) Returns() (r string) {
	if q.returns == nil || len(q.returns) == 0 {
		return
	}
	r = "RETURNING "
	for i, v := range q.returns {
		if i > 0 {
			r += ", "
		}
		r += v
	}
	return
}

func (q InsertQuery) Query() (string, []interface{}) {
	var p1, p2 string
	args := make([]interface{}, 0, len(q.cols))
	for i, v := range q.cols {
		if i > 0 {
			p1 += ", "
			p2 += ", "
		}
		p1 += v.Col
		p2 += "$" + strconv.FormatInt(int64(i+1), 10)
		args = append(args, v.Val)
	}
	sql := fmt.Sprintf("%s %s (%s) VALUES (%s) %s", q.Operation(), q.Table(), p1, p2, q.Returns())
	return sql, args
}
