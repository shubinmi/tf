package main

import (
	"bitbucket.org/shubinmi/tf/app/controller"
	"bitbucket.org/shubinmi/tf/app/repository"
	"bitbucket.org/shubinmi/tf/internal/conf"
	"bitbucket.org/shubinmi/tf/internal/migr"
	"bitbucket.org/shubinmi/tf/internal/transp"
	"bitbucket.org/shubinmi/tf/pkg/cb"
	"bitbucket.org/shubinmi/tf/pkg/pg"
	"context"
	"github.com/sirupsen/logrus"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func main() {

	domainCnf := conf.DomainConf()
	dbCnf := conf.DbConf()
	// TODO move to env
	grpcAddr := ":5544"

	logrus.SetLevel(logrus.InfoLevel)
	logrus.SetFormatter(&logrus.JSONFormatter{})

	// because db can be in running state
	err := cb.Call(func() error {
		return migr.PgDbMigrate(dbCnf)
	}, 10, 5*time.Second)
	if err != nil {
		panic(err)
	}

	wg := sync.WaitGroup{}
	ctx, cancel := context.WithCancel(context.Background())
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGTERM)

	db, err := pg.NewClient(dbCnf)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	// TODO add healthChecks + link with db.Status()

	repo := repository.NewUrl(db)
	ctrl := controller.NewUrl(repo, domainCnf.Host())

	wg.Add(1)
	go func() {
		_ = transp.GrpcServe(ctx, &wg, ctrl, grpcAddr)
		cancel()
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		select {
		case <-done:
			logrus.Info("starting Graceful Shutdown")
			cancel()
		case <-ctx.Done():
			return
		}
	}()

	wg.Wait()
}
