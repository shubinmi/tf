package conf

import (
	"github.com/caarlos0/env/v6"
	"github.com/pkg/errors"
)

type DB struct {
	Host string `env:"DBHOST" envDefault:"127.0.0.1"`
	Port string `env:"DBPORT" envDefault:"5433"`
	User string `env:"DBUSER" envDefault:"postgres"`
	Pass string `env:"DBPASS" envDefault:"postgres"`
	Db   string `env:"DBNAME" envDefault:"tf"`
	Ssl  bool   `env:"DBSSL"`
}

func (d *DB) SslMode() string {
	if d.Ssl {
		return "enable"
	}
	return "disable"
}

func DbConf() DB {
	cfg := DB{}
	if err := env.Parse(&cfg); err != nil {
		panic(errors.Wrap(err, "parse db env"))
	}
	return cfg
}
