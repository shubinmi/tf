package conf

import (
	"github.com/caarlos0/env/v6"
	"github.com/pkg/errors"
)

type Domain struct {
	Schema string `env:"DOMSCHEMA" envDefault:"http"`
	Name   string `env:"DOMSNAME" envDefault:"shrink.er"`
}

func (d *Domain) Host() string {
	return d.Schema + "://" + d.Name + "/"
}

func DomainConf() Domain {
	cfg := Domain{}
	if err := env.Parse(&cfg); err != nil {
		panic(errors.Wrap(err, "parse domain env"))
	}
	return cfg
}
