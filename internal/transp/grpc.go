package transp

import (
	urlapi "bitbucket.org/shubinmi/tf/api/proto"
	"bitbucket.org/shubinmi/tf/app/controller"
	"bitbucket.org/shubinmi/tf/internal/endpoints"
	"context"
	grpcmiddleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpclogrus "github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	grpcrecovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpcctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"net"
	"sync"
)

func GrpcServe(ctx context.Context, wg *sync.WaitGroup, cntr *controller.Url, grpcAddr string) error {

	lis, err := net.Listen("tcp", grpcAddr)
	if err != nil {
		return err
	}

	l := logrus.NewEntry(logrus.New())
	l.Logger.SetFormatter(&logrus.JSONFormatter{})
	s := grpc.NewServer(
		grpc.UnaryInterceptor(grpcmiddleware.ChainUnaryServer(
			grpcctxtags.UnaryServerInterceptor(),
			grpclogrus.UnaryServerInterceptor(l),
			grpcrecovery.UnaryServerInterceptor(),
		)),
	)
	urlapi.RegisterUrlServiceServer(s, endpoints.NewGrpcUrl(cntr))

	go func() {
		<-ctx.Done()
		s.GracefulStop()
		logrus.Info("gRPC Graceful Shutdown DONE")
		wg.Done()
	}()

	logrus.Info("starting gRPC")
	return s.Serve(lis)
}
