package migr

import (
	"bitbucket.org/shubinmi/tf/internal/conf"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"os"
)

func PgDbMigrate(cnf conf.DB) error {
	logrus.Info("starting migrations")
	curDir, err := os.Getwd()
	if err != nil {
		curDir = "."
	}
	m, err := migrate.New(
		"file://"+curDir+"/migration",
		"postgres://"+cnf.User+":"+cnf.Pass+"@"+cnf.Host+":"+cnf.Port+"/"+cnf.Db+"?sslmode="+cnf.SslMode())
	if err != nil {
		return errors.Wrap(err, "migrations init")
	}
	if err = m.Up(); err != nil && err != migrate.ErrNoChange {
		return errors.Wrap(err, "migrations up")
	}
	logrus.Info("migrations done")
	return nil
}
