package endpoints

import (
	urlapi "bitbucket.org/shubinmi/tf/api/proto"
	"bitbucket.org/shubinmi/tf/app/controller"
	"context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UrlService struct {
	urlapi.UnimplementedUrlServiceServer
	ctrl *controller.Url
}

func NewGrpcUrl(ctrl *controller.Url) *UrlService {
	return &UrlService{
		ctrl: ctrl,
	}
}

func (s *UrlService) Shrink(ctx context.Context, req *urlapi.UrlRequest) (*urlapi.UrlResponse, error) {
	link, err := s.ctrl.Shrink(ctx, req.GetUrl())
	if err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}
	res := &urlapi.UrlResponse{Url: link}
	return res, nil
}
