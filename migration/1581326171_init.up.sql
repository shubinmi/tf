CREATE TABLE IF NOT EXISTS urls
(
    id         serial PRIMARY KEY,
    address    text                     NOT NULL,
    alias      text                     NOT NULL,
    created_at timestamp with time zone NOT NULL DEFAULT NOW()
) WITHOUT OIDS;