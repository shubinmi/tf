all: clean proto dep test

build:
	docker build -t tf:latest -f ./build/Dockerfile .

buildlocal:
	GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o ./deployments/docker-compose/tf ./cmd/main.go

dep:
	go mod tidy

proto:
	@if ! which protoc > /dev/null; then \
		echo "error: protoc not installed" >&2; \
		exit 1; \
	fi
	protoc -I. \
		-I$(GOPATH)/src \
		--go_out=plugins=grpc:. ./api/proto/*.proto

newmigration:
	./script/create_migration.sh

test:
	go test ./...

clean:
	rm -rf ./api/proto/*.go

lint:
	golangci-lint run

.PHONY: \
	all \
	build \
	dep \
	proto \
	test \
	newmigration \
	lint \
	clean
